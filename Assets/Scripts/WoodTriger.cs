﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodTriger : MonoBehaviour
{
    public bool isDestroy = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerStay(Collider other)
    {
        if (isDestroy == true)
        {
            Destroy(other.gameObject);
        }
    }
}
