﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnMaterial : MonoBehaviour
{
    public WoodTriger woodSpawn;
    public GameObject[] instObjs;
    public GameObject material;
    public GameObject spawPlase;
    private float Xpos = -6f;
    public void Spawn()
    {
        woodSpawn.isDestroy = true;
        StartCoroutine(Wait());
        woodSpawn.isDestroy = false;
        for (int i = 0; i < instObjs.Length; i++)
        {
            
            Instantiate(instObjs[i], instObjs[i].transform.position, Quaternion.Euler(0, -47.6f, 0));
            Xpos += 0.5f;
        }

        Xpos = -6f;
    }

    public IEnumerator Wait()
    {
        yield return  new WaitForSeconds(0.5f);
    }
}
