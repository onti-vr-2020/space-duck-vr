﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 using Valve.VR.InteractionSystem;

 public class ForCutting : MonoBehaviour
{
    public int countOfCutted = 0;
    public int maxCountOfCut = 5;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool CanCut()
    {
        return countOfCutted <= maxCountOfCut && GetComponent<Interactable>().attachedToHand == null;
    }
}
