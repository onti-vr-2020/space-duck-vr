﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR.Extras;
using UnityEngine.EventSystems;

public class HandRaycast : MonoBehaviour
{
    public SteamVR_LaserPointer laser;

    public bool flag = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (laser.isClick)
        {
            if (flag) return;
            flag = true;
            Ray raycast = new Ray(transform.position, transform.forward);
            RaycastHit hit;
            if (Physics.Raycast(raycast, out hit))
            {
                if (hit.collider.gameObject.GetComponent<SphereCollider>())
                {
                    if (hit.collider.gameObject.tag == "Button")
                    {
                        hit.collider.gameObject.GetComponent<Button>().onClick.Invoke();
                    }
                }
            }
        }
        else flag = false;
    }
}
