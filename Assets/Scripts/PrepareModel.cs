﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrepareModel : MonoBehaviour
{
    public Camera screenshotCamera;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Compare()
    {
        string[] tags = { };
        foreach (string tag in tags)
        {
            GameObject.FindWithTag(tag).GetComponent<SetMaterial>().SetTargetMaterial(true);
        }

        double res = screenshotCamera.GetComponent<PhotoCapture>().CompareModels();
        
        foreach (string tag in tags)
        {
            GameObject.FindWithTag(tag).GetComponent<SetMaterial>().SetTargetMaterial(false);
        }
    }

    void CreateTargetScreenshot(GameObject lvl)
    {
        Compare();
        lvl.GetComponent<SetMaterial>().SetTargetMaterial(true);
        screenshotCamera.GetComponent<PhotoCapture>().CreateTargetScreenshot();
        lvl.GetComponent<SetMaterial>().SetTargetMaterial(false);
    }
}
