﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chisel : MonoBehaviour
{
    public int total = 0;
    public ChiselHead head;
    public ChiselBlade blade;
    public GameObject target;

    public void Hit()
    {
        if (target == null) return;
        total++;
        if (total != 2) return;
        blade.Cut(target);
        EndOfCutting();
    }

    public void StartOfCutting(GameObject aim)
    {
        target = aim;
    }

    public void EndOfCutting()
    {
        total = 0;
        target = null;
    }
}
