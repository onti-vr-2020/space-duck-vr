﻿using UnityEngine;
using UnityEngine.UI;

public class RadialProgress : MonoBehaviour {
    public Text ProgressIndicator;
    public Image LoadingBar;
    private float currentValue = 0;

    // Use this for initialization
    void Start () {
	    SetVisible(false);
    }
	
    // Update is called once per frame
    void Update () {

    }

    public bool AddValue(int value)
    {
        currentValue += value;
        UpdateUi();
        Debug.Log(currentValue);
        
        return currentValue >= 100;
    }

    private void UpdateUi()
    {
        if (currentValue <= 100)
        {
            ProgressIndicator.text = ((int) currentValue).ToString() + "%";
        }
        else
        {
            ProgressIndicator.text = "0%";
            currentValue = 0;
        }

        LoadingBar.fillAmount = currentValue / 100;
    }

    public void StartProgress()
    {
        Debug.Log("1");
        currentValue = 0;
        SetVisible(true);
    }

    private void SetVisible(bool value)
    {
        LoadingBar.enabled = value;
        ProgressIndicator.enabled = value;
    }
    public void EndProgress()
    {
        Debug.Log("0");
        currentValue = 0;
        SetVisible(false);
    }
}