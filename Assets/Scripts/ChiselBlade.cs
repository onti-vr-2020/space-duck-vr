﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChiselBlade : MonoBehaviour
{
    public void Cut(GameObject target)
    {
        GetComponent<CutMultiplePartsConcave>().target = target;
        GetComponent<CutMultiplePartsConcave>().prefabPart.tag = target.tag;
        GetComponent<CutMultiplePartsConcave>().prefabPart.GetComponent<Rigidbody>().mass =
            target.GetComponent<Rigidbody>().mass;
        GetComponent<CutMultiplePartsConcave>().Cut();
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag($"Stone"))
        {
            transform.parent.GetComponent<Chisel>().StartOfCutting(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag($"Stone"))
        {
            transform.parent.GetComponent<Chisel>().EndOfCutting();
        }
    }
}
