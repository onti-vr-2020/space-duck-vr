﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;

public class OpenLevel : MonoBehaviour
{
    public Text prog;
    public int level;
    public Sprite prev;
    public Image previuPlace;
    public Text lvlN;
    public List<GameObject> levels;

    public void Open()
    {
        lvlN.text = "lvl:" + level.ToString();
        BoardMenu parent = transform.parent.parent.GetComponent<BoardMenu>();
        prog.text = "Progress: " + ((int)parent.progresses[level-1]).ToString()+ "%";
        previuPlace.sprite = prev;
        List < int> test;

        for (int i = 0; i < levels.Count; i++)
        {
            levels[i].SetActive(i + 1 == level);
        }
    }
}
