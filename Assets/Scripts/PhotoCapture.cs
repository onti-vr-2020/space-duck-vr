﻿using System.Collections;
using UnityEngine;

public class PhotoCapture : MonoBehaviour
{
    private Vector3[] positions = new []
    {
        new Vector3(-4, 1.5f, -5.5f), new Vector3(-1.708f, 1.5f, -8.385f), new Vector3(1.08f, 1.5f, -5.633f), new Vector3(-1.396f, 1.5f, -3.018f),
        new Vector3(-4, 3f, -5.5f), new Vector3(-1.708f, 3f, -8.385f), new Vector3(1.08f, 3f, -5.633f), new Vector3(-1.396f, 3f, -3.018f),
        new Vector3(-4, 0.5f, -5.5f), new Vector3(-1.708f, 0.5f, -8.385f), new Vector3(1.08f, 0.5f, -5.633f), new Vector3(-1.396f, 0.5f, -3.018f),
        new Vector3(-3.495f, 1.5f, -3.495f), new Vector3(-3.495f, 1.5f, -7.461f), new Vector3(1.093f, 1.5f, -7.461f), new Vector3(1.081f, 1.5f, -2.947f),
    };
    public Texture2D[] targetScreenshots;

    private Vector3 target = new Vector3(-1.47f, 1.007f, -5.433124f);
    public Camera captureCamera;
    
    public Texture2D MakeScreenshot() {
        int width = captureCamera.pixelWidth;
        int height = captureCamera.pixelHeight;    
        Texture2D texture = new Texture2D(width, height);
     
        RenderTexture targetTexture = RenderTexture.GetTemporary(width, height);
        
        this.captureCamera.targetTexture = targetTexture;
        this.captureCamera.Render();
        
        RenderTexture.active = targetTexture;
     
        Rect rect = new Rect(0, 0, width, height);
        texture.ReadPixels(rect, 0, 0);
        texture.Apply();

        return texture;
    }

    double CompareImage(Texture2D texture, Texture2D targetTexture)
    {
        double countIdealPixels = 0;
        int  width = texture.width, height = texture.height;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (texture.GetPixel(x, y) == targetTexture.GetPixel(x, y)) countIdealPixels++;
            }
        }
        // Index Jaccard
        return countIdealPixels / (width * height * 2 - countIdealPixels);
    }

    public double CompareModels()
    {
        double res = 1, now;
        
        for (int i = 0; i < positions.Length; i++)
        {
            NewPosition(positions[i], target);
    
            now = CompareImage(MakeScreenshot(), targetScreenshots[i]);
    
            if (now < res) res = now;
        }

        return res;
    }

    public void NewPosition(Vector3 pos, Vector3 aim)
    {
        transform.position = pos;
        transform.LookAt(aim);
    }

    public void CreateTargetScreenshot()
    {
        targetScreenshots = null;
        for (int i = 0; i < positions.Length; i++)
        {
            NewPosition(positions[i], target);
            ((IList) targetScreenshots).Add(MakeScreenshot());
        }
    }
}
