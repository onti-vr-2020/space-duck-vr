﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SawScript : MonoBehaviour
{
    public float xPos;
    CutMultiplePartsConcave c;

    public GameObject progress;
    void Start()
    {
        xPos = transform.position.x;
        c = GetComponent<CutMultiplePartsConcave>();
    }
    

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Wood") && other.gameObject.GetComponent<ForCutting>().CanCut())
        {    
            c.target = other.gameObject;
            progress.GetComponent<RadialProgress>().StartProgress();
        }
    }
    
    public void OnTriggerExit(Collider other)      
    {
        if (other.gameObject.CompareTag("Wood"))
        {
            c.target = null;
            progress.GetComponent<RadialProgress>().EndProgress();
        }
    }

    public void OnTriggerStay(Collider other)
    {
        if (c.target == null) return;
        if (transform.position.x - xPos > 0.3 || -0.3 < transform.position.x - xPos)
        {
            bool res = progress.GetComponent<RadialProgress>().AddValue(1);
            xPos = transform.position.x;
            if (res)
            {
                c.prefabPart.tag = c.target.tag;
                c.prefabPart.GetComponent<Rigidbody>().mass = c.target.GetComponent<Rigidbody>().mass;
                c.mat.mainTexture = c.tex;
                c.Cut();
                c.target = null;
                
            }
        }

        
    }
}
