﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class klay : MonoBehaviour
{
    public SteamVR_Input_Sources action;
    public SteamVR_Action_Boolean flagAction;
    
    void Fasten(GameObject obj1, GameObject obj2)
    {
        obj2.GetComponent<Rigidbody>().useGravity = false;
        obj2.transform.SetParent(obj1.transform);
    }

    void Unfasten(GameObject obj1, GameObject obj2)
    {
        if (obj2.transform.parent == obj1.transform) obj2.transform.parent = null;
        else print("Obj1 is not parent of obj2");
    }
    
    public void OnCollisionStay(Collision other)
    {
        if ((other.gameObject.CompareTag($"Wood") || other.gameObject.CompareTag($"Place") ||
             other.gameObject.CompareTag($"Stone") || other.gameObject.CompareTag($"Steel")) && 
            other.gameObject.GetComponent<Rigidbody>().useGravity == false &&
            GetComponent<Interactable>().attachedToHand != null && flagAction.GetStateDown(action))
        {
            Fasten(other.gameObject, gameObject);
        }
    }
}