﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HammerHead : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag($"ChiselHead"))
        {
            other.gameObject.GetComponent<ChiselHead>().Hit();
        }    
    }

}
