﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Broom : MonoBehaviour
{
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Wood") || other.gameObject.CompareTag($"Stone") || other.gameObject.CompareTag($"Steel"))
        {
            for (int i = 0; i < other.gameObject.transform.childCount; i++) other.gameObject.transform.GetChild(i).parent = null;
            Destroy(other.gameObject);
        }
    }
}
