﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class nwe : MonoBehaviour
{
    public void OnCollisionEnter(Collision other)
    {
        Debug.Log(other.gameObject.name);
        if (other.gameObject.tag == "Wood" || other.gameObject.tag == "Stone" || other.gameObject.tag == "Steel")
        {
            Destroy(other.gameObject);
        }
    }
}
