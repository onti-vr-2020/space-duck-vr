﻿using  System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;


public class Glue : MonoBehaviour
{
    void Fasten(GameObject obj1, GameObject obj2)
    {
        obj2.GetComponent<Rigidbody>().useGravity = false;
        obj2.transform.SetParent(obj1.transform);
    }

    void Unfasten(GameObject obj1, GameObject obj2)
    {
        if (obj2.transform.parent == obj1.transform) obj2.transform.parent = null;
        else print("Obj1 is not parent of obj2");
    }
}