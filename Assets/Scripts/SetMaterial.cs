﻿using UnityEngine;

public class SetMaterial : MonoBehaviour
{
    private Material my;
    public Material target;
    // Start is called before the first frame update
    void Start()
    {
        my = GetComponent<Renderer>().material;
        SetTargetMaterial(true);
    }

    public void SetTargetMaterial(bool value)
    {
        GetComponent<Renderer>().material = value ? target : my;
    }
}
