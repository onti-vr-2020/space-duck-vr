﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChiselHead : MonoBehaviour
{
    public void Hit()
    {
        transform.parent.GetComponent<Chisel>().Hit();
    }
}
